# 2018 Roadmap

This road map was created as a set of personal goals to achieve each month within 2018. Overall I want to transition from a PHP/WordPress/Laravel developer to a JavaScript based developer using ReactJS.

As an incentive to complete this challenge, If I reach the target goal of the month I will reward myself with something.

## January

* Produce a hacker news clone in ReactJS :x:
* Read through "The road to learn React" 200 page PDF :x:

Reward for completion - Meal at Wagamma :ramen:

## Febuary

* 2018 roadmap website built using the WP rest API as a backend and React as a frontend :x:
* Complete Wes Bos JavaScript 30 course :x:
* Read "Launch it" :x:
* Complete Wes Bos CSS Grid course :x:

Reward for completion - TBC

## March

* Weather Forcecast app in NodeJS :x:
* Release custom WP starter theme and publish Medium post about modern WordPress development :x:
